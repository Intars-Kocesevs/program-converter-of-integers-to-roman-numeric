## Name
Converter of integers to Roman numeric

## Description
Made with C++. This is a simple console program.
Program converts integers to corresponding Roman numerics (in range of classical limit of integers from 1...3999).
![alt text](screenshot_-_integer-to-roman-converter.jpg "screenshot of converter in action")

## Installation
Program was originally compiled using mingw64 c++ compiler in Code::Blocks 17.12 IDE.
Provided source-code can be used to compile it on other setup/machines.

## Usage
Run executable named "integer_to_roman_numeric_converter.exe" to launch a program.
Also program can be compiled and run directly from source code in IDE that supports C++ compilation.

## Support
kocesevs.intars@gmail.com

## Roadmap
(currently not planned)

## License
Apache 2.0
