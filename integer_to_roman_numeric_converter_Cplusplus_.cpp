// Integer number convertor to Roman numerics (only up to standart/'classical' Roman numeric limit of 3999).
// (Intars K., August 2022)

//      Roman numerics
//      --------------   -----------
//      I           1  | I         1
//      V           5  | II        2
//      X          10  | III       3
//      L          50  | IV        4
//      C         100  | V         5
//      D         500  | VI        6
//      M        1000  | VII       7
//                     | VIII      8
//                     | IX        9

#include "iostream"
#include "stdlib.h"
#include <string>"
using namespace std;

int main()
{
    // ---------- console welcome screen   -----------------------------
    string console_welcome_banner =
    "  -----------------------------------------------------------------\n \
|                                                                 |\n \
|                Integer--Roman numeric converter                 |\n \
|                                                                 |\n \
|                                                                 |\n \
|         .   .   __            ___    __ __   ___ ___ ___        |\n \
|        /|  /|  /  \\   ____   /   \\    \\ /     |   |   |         |\n \
|         |   |   --/   ____   |         x      |   |   |         |\n \
|         |   |  \\__/          \\___/   _/ \\_   _|_ _|_ _|_        |\n \
|                                                                 |\n \
|                                                                 |\n \
 -----------------------------------------------------------------  \n";

    cout << console_welcome_banner;
    cout << " Program converts positive integers into Roman numerics.\n";
    cout << " (Range of accepted positive integers is limited up to a value of 3999)\n\n";
    cout << " Enter positive integer of interest for conversion:  ";
    int user_input = 0;
    cin >> user_input;
    if (cin.fail() )
    {   cout << " error: wrong input -- use only integer numbers (0...3999) \n";

        // for pausing automatic program exit
        string x;
        cout << "\n\nPress any symbolic/numeric key and hit Enter to exit...";
        cin.clear();    // reseting console input
        cin >> x;
        return EXIT_FAILURE;
    }
    if (user_input <= 0)
    {   cout << " error: wrong input -- use only positive integers ( i>0 ) \n";

        // for pausing automatic program exit
        string x;
        cout << "\n\n Press any symbolic/numeric key and hit Enter to exit...";
        cin >> x;
        return EXIT_FAILURE;
    }
    if (user_input > 3999)
    {   cout << "\n warning: input can't exceed value 3999 \n";

        // for pausing automatic program exit
        string x;
        cout << "\n\n Press any symbolic/numeric key and hit Enter to exit...";
        cin >> x;
        return EXIT_FAILURE;
    }

    // processing user_input
    int thousands = user_input / 1000;  // extracting the count of the thousands
    int remainder = user_input % 1000;
    int five_hundred = remainder / 500;    // calculate if any 500 fit into hundreds portion
    remainder = remainder % 500;
    int hundreds = remainder / 100; // extracting count of remaining hundreds
    int hundreds_total = 5 + hundreds;   // for tracking "900" situation
    remainder = remainder % 100;
    int fifty = remainder / 50;
    remainder = remainder % 50;
    int tens = remainder / 10;
    int tens_total = 5 + tens;  // for tracking situation "90"
    remainder = remainder % 10;
    int five = remainder / 5;
    remainder = remainder % 5;
    int ones = remainder;
    int ones_total = 5 + ones;   // for tracking situation "9"

    string roman_character_sequence = "";   // initialization

    // conversion to Roman numerics:
    // ----------------------------------------  1000s
    if (thousands > 0)
    {   if (thousands == 1) { roman_character_sequence = " M";   }   // 1000
        else if (thousands == 2) { roman_character_sequence = " MM"; }   // 2000
        else if (thousands == 3) { roman_character_sequence = " MMM"; }  // 3000
    }

    // ----------------------------------------  500s
    if (five_hundred == 0 and hundreds == 4)   // if there is one 500 or there is 400
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "CD";   // 400
    }
    else if (hundreds_total == 9)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "CM";   // 900
    }
    else if (five_hundred == 1)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "D";   // 500
    }

    // ----------------------------------------  100s
    if (hundreds > 0 and hundreds < 4 and five_hundred == 0)
    {   if (hundreds == 1) { roman_character_sequence += " | C";   }   // 100
        else if (hundreds == 2) { roman_character_sequence += " | CC"; }   // 200
        else if (hundreds == 3) { roman_character_sequence += " | CCC"; }  // 300
    }
    if (hundreds > 0 and hundreds < 4 and five_hundred == 1)
    {   if (hundreds == 1) { roman_character_sequence += "C";   }   // 100
        else if (hundreds == 2) { roman_character_sequence += "CC"; }   // 200
        else if (hundreds == 3) { roman_character_sequence += "CCC"; }  // 300
    }

    // ----------------------------------------  50
    if (fifty == 0 and tens == 4)   // if there is 40
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "XL";   // 40
    }
    else if (tens_total == 9)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "XC";   // 90
    }
    else if (fifty == 1)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "L";   // 50
    }

    // ----------------------------------------  10s
    if (tens > 0 and tens < 4 and fifty == 0)
    {   if (tens == 1) { roman_character_sequence += " | X";   }   // 10
        else if (tens == 2) { roman_character_sequence += " | XX"; }   // 20
        else if (tens == 3) { roman_character_sequence += " | XXX"; }  // 30
    }
    // formating specific to allow writing LXXX  (without delimiters)
    if (tens > 0 and tens < 4 and fifty == 1)
    {   if (tens == 1) { roman_character_sequence += "X";   }   // 10
        else if (tens == 2) { roman_character_sequence += "XX"; }   // 20
        else if (tens == 3) { roman_character_sequence += "XXX"; }  // 30
    }

    // ----------------------------------------  5
    if (five == 0 and ones == 4)   // if there is 4
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "IV";   // 4
    }
    else if (ones_total == 9)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "IX";   // 9
    }
    else if (five == 1)
    {   roman_character_sequence += " | ";  // adding delimiter space
        roman_character_sequence += "V";   // 5
    }

    // ----------------------------------------  1s
    if (ones > 0 and ones < 4 and five == 0)
    {   if (ones == 1) { roman_character_sequence += " | I";   }   // 1
        else if (ones == 2) { roman_character_sequence += " | II"; }   // 2
        else if (ones == 3) { roman_character_sequence += " | III"; }  // 3
    }
    // formating specific to allow writing VIII (without delimiters)
    if (ones > 0 and ones < 4 and five == 1)
    {   if (ones == 1) { roman_character_sequence += "I";   }   // 1
        else if (ones == 2) { roman_character_sequence += "II"; }   // 2
        else if (ones == 3) { roman_character_sequence += "III"; }  // 3
    }

    // printing final result
    cout << "\n" << roman_character_sequence;

    string x;
    cout << "\n\n Press any symbolic/numeric key and hit Enter to exit...";
    cin >> x;
    return EXIT_SUCCESS;
}
